# Kambista qa challenge 👋!

En Kambista estamos en búsqueda de una persona que nos ayude a ofrecer un servicio de calidad.  Queremos conocer un poco más tus habilidades y nos gustaría que puedes realizar el siguiente #qachallenge 🐞

## Caso

En Kambista surgió el requerimiento de realizar un nuevo proceso de autenticación. Nuestro scrum master registro las siguientes historias:

 - Nuevo proceso de login
 - Nuevo proceso de registro

El back y el front realizaron su parte ingresando las tareas que les corresponden a cada uno y realizándolas, ahora te toca a ti 🙌.

### Requerimientos
Los requerimientos entregados al equipo fueron los siguientes:
 
    - /register (POST)
	    body: {
			email => requerido , minimo un @
			password => requerido , minimo 5 caracteres 
		}
		response: 
			- status (200) : {
				code:  201,
				msg:  'SAVED'
			}
			- status (422 - paramentros requeridos no enviados) : {
				code:  999,
				msg:  'REQUERIDO'
			}
			- status (422 - paramentros invalidos) : {
				code:  998,
				msg:  'INVALID'
			}
	- /login (POST)
	    body: {
			email => requerido
			password => requerido
		}
		response: 
			- status (200) : {
				code:  201,
				msg:  'LOGIN'
			}
			- status (422 - paramentros requeridos no enviados) : {
				code:  999,
				msg:  'REQUERIDO'
			}
			- status (401 - credenciales invalidas) : {
				code:  997,
				msg:  'INVALID'
			}

### Consideraciones

En Kambista se tiene los siguientes ambientes de trabajo:

```mermaid
sequenceDiagram
Dev ->> Qa: .....
Qa ->> Prod: .....
Qa ->> Dev: .....
Prod ->> Qa: .....
```

> Puede sugerirse otra estructura

Tambien se sigue un flujo de CI/CD

```mermaid
graph LR
A((local)) -- ??? --> B
B[Git]-- ??? --> C((Dev))
C -- ?? --> D((Qa))
D -- ?? --> E((Prod))
```
> Puede sugerirse otra estructura

### Pasos

Clona el repositorio 🗂y levanta🔥los servicios :

    $ git clone https://gitlab.com/kambista-public/qa-challenge.git
    $ cd qa-challenge
    $ cd back/
    $ npm run start
    $ cd ..
    $ cd front/
    $ npm run start

### Objetivos


 1. Identifica tareas y/o casos de prueba que estarían enlazadas a las historias creadas para el proceso de pruebas. ( qa-challenge-[nombre].docx )
 2. Implementa los tipos de pruebas basicos o necesarios que consideres necesarias ( unitarias, servicios, UI y manuales), de tal manera que se pueda evaluar los requerimientos solicitados en las historias. Se debe crea una carpeta llamada test en la raiz del proyecto.
 4. Comentar sobre el proceso actual de trabajo y los ambientes. ( qa-challenge-[nombre].docx )
 5. Sugerir en qué ambiente y en qué momento del flujo de trabajo se deben ejecutar cada tipo de pruebas.  ( qa-challenge-[nombre].docx )
 6. Publícalo en el repositorio de tu preferencia y envíanos un correo con el link y el documento ( qa-challenge-[nombre].docx ) a: paulo@kambista.com
 
Exitos y gracias por tu tiempo!😃

Ante cualquier duda puedes enviar un correo a paulo@kambista.com y estaremos encantados de ayudarte. 